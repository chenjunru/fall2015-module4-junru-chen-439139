#!/usr/bin/env python

import re
import sys,os
import operator

if len(sys.argv)<2:
    sys.exit("Usage:%s filename" %sys.argv[0])
filename=sys.argv[1]

if not os.path.exists(filename):
    sys.exit("Error:File '%s' not found" %sys.argv[1])


stat_dict={}
percentage_dict={}
sorted_dict={}

_name = re.compile(r"\b[A-Z]\w{1,10} [A-Z]\w{1,10}\b(?= b)")
_bat = re.compile(r"\b\d\b(?= times)")
_hits = re.compile(r"\b\d\b(?= hits)")

f = open(filename)
for line in f:
    line = line.rstrip()

    name = re.search(_name,line)
    bat_time = re.search(_bat,line)
    hits_time = re.search(_hits,line)

    if name:
        if name.group() not in stat_dict:
            stat_dict[name.group()]=[int(bat_time.group()),int(hits_time.group())]
        else:
            oldarray = None
            oldarray = stat_dict[name.group()]
            stat_dict[name.group()]=[oldarray[0]+int(bat_time.group()),oldarray[1]+int(hits_time.group())]

for stat in stat_dict:

    percentage_dict[stat] = round(float(stat_dict[stat][1])/float(stat_dict[stat][0]),3)

  
sorted_dict=sorted(percentage_dict.items(), key=operator.itemgetter(1),reverse = True)

for keys,values in sorted_dict:
    print keys,values

 
f.close()

with open("out.txt", 'w') as file:
    for item in sorted_dict:
        file.write("{}\n".format(item))
    